using System.Collections.Generic;
using Entitas;

public class SoundFlyTapSystem : ReactiveSystem<GameEntity>
{
    private GameContext _game;
    public SoundFlyTapSystem(Contexts contexts) : base(contexts.game)
    {
        _game = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Jump);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isBird;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        AudioHandler.Instance.PlayFlyTap();
    }
}