using System.Collections.Generic;
using Entitas;

public class SoundHitSystem : ReactiveSystem<GameEntity>
{
    public SoundHitSystem(Contexts contexts) : base(contexts.game)
    {
        
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Dead);
    }

    protected override bool Filter(GameEntity entity)
    {
        return true;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        AudioHandler.Instance.PlayHit();
    }
}