using Entitas;

public class AudioSystems : Feature
{
    public AudioSystems(Contexts contexts) : base("Audio Systems")
    {
        Add(new SoundHitSystem(contexts));
        Add(new SoundPointSystem(contexts));
        
        Add(new SoundFlyTapSystem(contexts));
    }         
}