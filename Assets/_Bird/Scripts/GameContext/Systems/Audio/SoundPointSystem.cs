using System.Collections.Generic;
using Entitas;

public class SoundPointSystem : ReactiveSystem<GameEntity>
{
    public SoundPointSystem(Contexts contexts) : base(contexts.game)
    {
        
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.GainedPoint);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isGainedPoint;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        AudioHandler.Instance.PlayPoint();
    }
}