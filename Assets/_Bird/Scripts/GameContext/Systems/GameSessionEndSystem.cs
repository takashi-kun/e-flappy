using System.Collections.Generic;
using Entitas;
using Entitas.Unity;

public class GameSessionEndSystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext _game;
    public GameSessionEndSystem(Contexts contexts) : base(contexts.game)
    {
        _game = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Dead);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isBird;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var e in entities)
        {
            if (e.hasView)
            {
                var view = e.view.gameObject;
                view.Unlink();
            }
            
            e.Destroy();
        }
        
        _game.isControllable = false;
        _game.ReplaceGameSession(GameStatus.End);
    }
}