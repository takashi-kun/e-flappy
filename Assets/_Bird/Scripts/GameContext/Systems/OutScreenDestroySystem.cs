using Entitas;
using UnityEngine;

public class OutScreenDestroySystem : IExecuteSystem
{
    private readonly IGroup<GameEntity> _group;
    private float _threshold;

    public OutScreenDestroySystem(Contexts contexts)
    {
        _group = contexts.game.GetGroup(GameMatcher
            .AllOf(GameMatcher.Position)                // only position
            .NoneOf(GameMatcher.Destroyed));
        
        GetScreenWidth();
    }

    private void GetScreenWidth()
    {
        var camera = Camera.main;
        if (camera != null)
        {
            var aspect = camera.aspect;
            var screenHalfWidth = camera.orthographicSize * aspect;
            _threshold = -screenHalfWidth - 1f;
        }
    }

    public void Execute()
    {
        if (_threshold > 0) return;
        foreach (GameEntity e in _group.GetEntities())
        {
            if (e.position.value.x <= _threshold)
            {
                e.isDestroyed = true;
            }
        }
    }
}