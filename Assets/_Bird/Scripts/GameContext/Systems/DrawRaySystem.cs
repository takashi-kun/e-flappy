using Entitas;
using UnityEngine;

public class DrawRaySystem : IExecuteSystem
{
    private float _rayLength = 0.1f;
    private readonly IGroup<GameEntity> _group;

    public DrawRaySystem(Contexts contexts)
    {
        _group = contexts.game.GetGroup(GameMatcher.Ray);
    }

    public void Execute()
    {
        foreach (GameEntity e in _group)
        {
            var center = e.ray.collider.bounds.center;
            foreach (var direction in e.ray.directions)
            {
                switch (direction)
                {
                    case RayDirection.Up:
                        Vector3 pUp = center;
                        pUp.y += e.ray.offset;
                        DrawRay(direction, pUp, Vector3.up);
                        break;

                    case RayDirection.Down:
                        Vector3 pDown = center;
                        pDown.y -= e.ray.offset;
                        DrawRay(direction, pDown, Vector3.down);
                        break;

                    case RayDirection.Left:
                        Vector3 pLeft = center;
                        pLeft.x -= e.ray.offset;
                        DrawRay(direction, pLeft, Vector3.left);
                        break;

                    case RayDirection.Right:
                        Vector3 pRight = center;
                        pRight.x += e.ray.offset;
                        DrawRay(direction, pRight, Vector3.right);
                        break;
                }
            }
        }
    }

    private void DrawRay(RayDirection direction, Vector3 origin, Vector2 vectorDirection)
    {
        bool horizontal = direction == RayDirection.Left || direction == RayDirection.Right;
        for (int i = -1; i <= 1; i++)
        {
            var xOrigin = origin;
            if (horizontal)
            {
                xOrigin.y += i * 0.1f;
            }
            else
            {
                xOrigin.x += i * 0.1f;
            }
            
            Debug.DrawRay(xOrigin, vectorDirection * _rayLength, Color.red);
        }
    }
}