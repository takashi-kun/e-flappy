using Entitas;

public class GameSystems : Feature
{
    public GameSystems(Contexts contexts) : base("Game Systems")
    {
        //Init, Events
        Add(new PointEventSystem(contexts));
        Add(new GameSessionEventSystem(contexts));
        
        //Input      
        Add(new JumpSystem(contexts));
        
        //Update
        
        Add(new PhysicSystem(contexts));
        Add(new BirdHitSystem(contexts));
              
        Add(new PipeSpawnerSystem(contexts));
        Add(new PipeMovingSystem(contexts));

        Add(new OutScreenDestroySystem(contexts));
        Add(new CalculatePointSystem(contexts));
        
        //View/Render
        Add(new RenderPositionSystem(contexts));
        Add(new RenderDynamicBodySystem(contexts));
        
        
        Add(new DrawRaySystem(contexts));
        
        
        //Clean up, reactive
        Add(new PointSystem(contexts));
        
        Add(new GameSessionEndSystem(contexts));
        Add(new GameSessionReplaySystem(contexts));  
        
        Add(new DestroySystem(contexts));
        Add(new GameCleanupSystems(contexts));
    }         
}