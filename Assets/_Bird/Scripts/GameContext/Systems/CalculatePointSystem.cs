using Entitas;

public class CalculatePointSystem : IExecuteSystem
{
    private float _offset = 0.5f;
    private readonly IGroup<GameEntity> _group;

    public CalculatePointSystem(Contexts contexts)
    {
        _group = contexts.game.GetGroup(GameMatcher
            .AllOf(GameMatcher.Pipe, GameMatcher.Position)
            .NoneOf(GameMatcher.GainedPoint));
    }

    public void Execute()
    {
        foreach (GameEntity e in _group.GetEntities())
        {
            if (e.position.value.x <= -_offset)
            {
                e.isGainedPoint = true;
            }
        }
    }
}