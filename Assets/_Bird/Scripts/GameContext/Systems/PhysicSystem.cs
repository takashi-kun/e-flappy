using Entitas;
using UnityEngine;

public class PhysicSystem : IExecuteSystem
{
    public const float GRAVITY = -10f;
    
    private readonly IGroup<GameEntity> _bodyGroup;

    public PhysicSystem(Contexts contexts)
    {
        _bodyGroup = contexts.game.GetGroup(GameMatcher.Velocity);
    }

    public void Execute()
    {
        foreach (var e in _bodyGroup)
        {
            Vector3 velocity = e.velocity.value;
            
            velocity.y += GRAVITY * Time.deltaTime;
            
            e.ReplaceVelocity(velocity);
        }
    }
}