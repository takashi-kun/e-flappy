using System.Collections.Generic;
using Entitas;
using Entitas.Unity;

public class PointSystem : ReactiveSystem<GameEntity>
{
    private GameContext _game;
    public PointSystem(Contexts contexts) : base(contexts.game)
    {
        _game = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.GainedPoint);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isGainedPoint;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        var point = _game.point.value;
        point++;
        _game.ReplacePoint(point);
    }
}