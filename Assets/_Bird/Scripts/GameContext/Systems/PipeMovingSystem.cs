using Entitas;
using UnityEngine;

public class PipeMovingSystem : IExecuteSystem
{
    private const float SPEED = 4f;
    private readonly GameContext _game;
    private readonly IGroup<GameEntity> _group;

    public PipeMovingSystem(Contexts contexts)
    {
        _game = contexts.game;
        _group = contexts.game.GetGroup(GameMatcher
                 .AllOf(GameMatcher.Pipe, GameMatcher.Position)
                 .NoneOf(GameMatcher.Destroyed));
    }

    public void Execute()
    {
        if (_game.isControllable)
        {
            foreach (GameEntity e in _group.GetEntities())
            {
                var position = e.position.value;
                position.x -= SPEED * Time.deltaTime;
                e.ReplacePosition(position);
            }
        }
    }
}