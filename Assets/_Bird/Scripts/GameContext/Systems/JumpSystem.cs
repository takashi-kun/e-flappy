using System.Collections.Generic;
using Entitas;

public class JumpSystem : ReactiveSystem<GameEntity>
{
    private float _jumpVelocity = 4f;
    private GameContext _game;
    public JumpSystem(Contexts contexts) : base(contexts.game)
    {
        _game = contexts.game;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Jump);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasVelocity;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        if (!_game.isControllable) return;
        
        foreach (GameEntity e in entities)
        {
            var vel = e.velocity.value;
            vel.y = _jumpVelocity;
            e.ReplaceVelocity(vel);
        }
    }
}