using System.Collections.Generic;
using Entitas;

public class GameSessionReplaySystem : ReactiveSystem<GameEntity>
{
    private readonly GameContext _context;
    private readonly IGroup<GameEntity> _pipeGroup;

    public GameSessionReplaySystem(Contexts contexts) : base(contexts.game)
    {
        _context = contexts.game;
        _pipeGroup = _context.GetGroup(GameMatcher.AllOf(GameMatcher.Pipe).NoneOf(GameMatcher.Destroyed));
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
       return context.CreateCollector(GameMatcher.GameSession);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasGameSession;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        if (_context.gameSessionEntity.gameSession.status == GameStatus.Prepare)
        {
            foreach (var pipe in _pipeGroup.GetEntities())
            {
                pipe.isDestroyed = true;
            }
        }
    }
}