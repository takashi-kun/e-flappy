using Entitas;
using Entitas.Unity;
using UnityEngine;

public class PipeSpawnerSystem : IExecuteSystem
{
    private readonly float _pipesGap = 1.5f;
    private readonly float _gapTime = 1f;
    
    private GameContext _game;
    private GameObject _pipePrefab;
    
    private float _spawnPositionX;
    private float _timer;

    public PipeSpawnerSystem(Contexts contexts)
    {
        _game = contexts.game;
        GetSpawnPosition();

        _pipePrefab = Resources.Load<GameObject>(@"Prefabs/Pipe");
    }
    
    private void GetSpawnPosition()
    {
        var camera = Camera.main;
        if (camera != null)
        {
            var aspect = camera.aspect;
            var screenHalfWidth = camera.orthographicSize * aspect;
            _spawnPositionX = screenHalfWidth + 2f;
        }
    }

    public void Execute()
    {
        if (_game.isControllable)
        {
            _timer += Time.deltaTime;
            if (_timer >= _gapTime)
            {
                _timer = 0;

                CreatePipe();
            }
        }
    }

    private void CreatePipe()
    {
        var pos = new Vector3(_spawnPositionX, Random.Range(-1.5f, 2.5f), 0f);
        var goPipe = Lean.Pool.LeanPool.Spawn(_pipePrefab, pos, Quaternion.identity);
        
        var entity = _game.CreateEntity();
        entity.isPipe = true;
        entity.AddView(goPipe);
        entity.AddPosition(pos);
        goPipe.Link(entity);
    }
}