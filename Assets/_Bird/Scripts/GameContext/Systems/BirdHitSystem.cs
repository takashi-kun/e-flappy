using Entitas;
using UnityEngine;

public class BirdHitSystem : IExecuteSystem
{
    private float _rayLength = 0.05f;
    private int _mask = 1 << 7;
    private readonly IGroup<GameEntity> _group;

    public BirdHitSystem(Contexts contexts)
    {
        _group = contexts.game.GetGroup(GameMatcher
            .AllOf(GameMatcher.Bird, GameMatcher.Velocity, GameMatcher.Ray)
            .NoneOf(GameMatcher.Dead));
    }

    public void Execute()
    {
        foreach (GameEntity e in _group.GetEntities())
        {
            foreach (var direction in e.ray.directions)
            {
                switch (direction)
                {
                    case RayDirection.Up:
                        Vector3 pUp = e.ray.collider.bounds.center;
                        pUp.y += e.ray.offset;
                        SetRay(e, pUp, direction, Vector2.up);
                        break;

                    case RayDirection.Down:
                        Vector3 pDown = e.ray.collider.bounds.center;
                        pDown.y -= e.ray.offset;
                        SetRay(e, pDown, direction, Vector2.down);
                        break;
                    
                    case RayDirection.Left:
                        Vector3 pLeft = e.ray.collider.bounds.center;
                        pLeft.x -= e.ray.offset;
                        SetRay(e, pLeft, direction, Vector2.left);
                        break;
                    
                    case RayDirection.Right:
                        Vector3 pRight = e.ray.collider.bounds.center;
                        pRight.x += e.ray.offset;
                        SetRay(e, pRight, direction, Vector2.right);
                        break;
                }

                if (e.isDead) break;
            }
        }
    }

    private void SetRay(GameEntity e, Vector3 origin, RayDirection direction, Vector2 vectorDirection)
    {
        bool horizontal = direction == RayDirection.Left || direction == RayDirection.Right;
        for (int i = -1; i <= 1; i++)
        {
            var xOrigin = origin;
            if (horizontal)
            {
                xOrigin.y += i * 0.1f;
            }
            else
            {
                xOrigin.x += i * 0.1f;
            }
            
            RaycastHit2D hit = Physics2D.Raycast(xOrigin, vectorDirection, _rayLength, _mask);

            if (hit)
            {
                e.isDead = true;
                e.RemoveVelocity();
                break;
            }
        }
    }
}