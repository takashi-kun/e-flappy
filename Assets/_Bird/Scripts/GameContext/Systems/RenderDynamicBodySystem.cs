using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class RenderDynamicBodySystem : ReactiveSystem<GameEntity>
{
    public RenderDynamicBodySystem(Contexts contexts) : base(contexts.game)
    {
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Velocity);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasVelocity && entity.hasView;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (GameEntity e in entities)
        {
            e.view.gameObject.transform.Translate(e.velocity.value * Time.deltaTime);
        }
    }
}