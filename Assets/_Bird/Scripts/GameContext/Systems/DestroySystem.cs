using System.Collections.Generic;
using Entitas;
using Entitas.Unity;

public class DestroySystem : ReactiveSystem<GameEntity>
{
    public DestroySystem(Contexts contexts) : base(contexts.game)
    {
        
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Destroyed);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isDestroyed;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (GameEntity e in entities)
        {
            if (e.hasView)
            {
                var go = e.view.gameObject;
                go.Unlink(); //must unlink from gameObject.

                Lean.Pool.LeanPool.Despawn(go); // pool the view
            }
            
            e.Destroy(); //destroy all from all retained -> put into entity reusable pool
        }
    }
}