using Entitas;
using UnityEngine;

[Game]
public sealed class CollisionComponent : IComponent
{
    public Collider2D collider;
}