using Entitas;
using UnityEngine;

public enum RayDirection
{
    Up,
    Down,
    Left,
    Right
}

[Game]
public sealed class RayComponent : IComponent
{
    public Collider2D collider;
    public RayDirection[] directions;
    public float offset;
}