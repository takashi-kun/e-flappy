using Entitas;
using Entitas.CodeGeneration.Attributes;

public enum GameStatus
{
    Start,
    Prepare,
    Playing,
    End
}

[Game, Unique, Event(EventTarget.Self)]
public sealed class GameSessionComponent : IComponent
{
    public GameStatus status;
}