//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public RayComponent ray { get { return (RayComponent)GetComponent(GameComponentsLookup.Ray); } }
    public bool hasRay { get { return HasComponent(GameComponentsLookup.Ray); } }

    public void AddRay(UnityEngine.Collider2D newCollider, RayDirection[] newDirections, float newOffset) {
        var index = GameComponentsLookup.Ray;
        var component = (RayComponent)CreateComponent(index, typeof(RayComponent));
        component.collider = newCollider;
        component.directions = newDirections;
        component.offset = newOffset;
        AddComponent(index, component);
    }

    public void ReplaceRay(UnityEngine.Collider2D newCollider, RayDirection[] newDirections, float newOffset) {
        var index = GameComponentsLookup.Ray;
        var component = (RayComponent)CreateComponent(index, typeof(RayComponent));
        component.collider = newCollider;
        component.directions = newDirections;
        component.offset = newOffset;
        ReplaceComponent(index, component);
    }

    public void RemoveRay() {
        RemoveComponent(GameComponentsLookup.Ray);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherRay;

    public static Entitas.IMatcher<GameEntity> Ray {
        get {
            if (_matcherRay == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.Ray);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherRay = matcher;
            }

            return _matcherRay;
        }
    }
}
