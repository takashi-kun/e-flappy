//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public GameSessionListenerComponent gameSessionListener { get { return (GameSessionListenerComponent)GetComponent(GameComponentsLookup.GameSessionListener); } }
    public bool hasGameSessionListener { get { return HasComponent(GameComponentsLookup.GameSessionListener); } }

    public void AddGameSessionListener(System.Collections.Generic.List<IGameSessionListener> newValue) {
        var index = GameComponentsLookup.GameSessionListener;
        var component = (GameSessionListenerComponent)CreateComponent(index, typeof(GameSessionListenerComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplaceGameSessionListener(System.Collections.Generic.List<IGameSessionListener> newValue) {
        var index = GameComponentsLookup.GameSessionListener;
        var component = (GameSessionListenerComponent)CreateComponent(index, typeof(GameSessionListenerComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemoveGameSessionListener() {
        RemoveComponent(GameComponentsLookup.GameSessionListener);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherGameSessionListener;

    public static Entitas.IMatcher<GameEntity> GameSessionListener {
        get {
            if (_matcherGameSessionListener == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.GameSessionListener);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherGameSessionListener = matcher;
            }

            return _matcherGameSessionListener;
        }
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.EventEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public void AddGameSessionListener(IGameSessionListener value) {
        var listeners = hasGameSessionListener
            ? gameSessionListener.value
            : new System.Collections.Generic.List<IGameSessionListener>();
        listeners.Add(value);
        ReplaceGameSessionListener(listeners);
    }

    public void RemoveGameSessionListener(IGameSessionListener value, bool removeComponentWhenEmpty = true) {
        var listeners = gameSessionListener.value;
        listeners.Remove(value);
        if (removeComponentWhenEmpty && listeners.Count == 0) {
            RemoveGameSessionListener();
        } else {
            ReplaceGameSessionListener(listeners);
        }
    }
}
