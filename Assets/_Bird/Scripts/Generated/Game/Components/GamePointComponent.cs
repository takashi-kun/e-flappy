//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentContextApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameContext {

    public GameEntity pointEntity { get { return GetGroup(GameMatcher.Point).GetSingleEntity(); } }
    public PointComponent point { get { return pointEntity.point; } }
    public bool hasPoint { get { return pointEntity != null; } }

    public GameEntity SetPoint(int newValue) {
        if (hasPoint) {
            throw new Entitas.EntitasException("Could not set Point!\n" + this + " already has an entity with PointComponent!",
                "You should check if the context already has a pointEntity before setting it or use context.ReplacePoint().");
        }
        var entity = CreateEntity();
        entity.AddPoint(newValue);
        return entity;
    }

    public void ReplacePoint(int newValue) {
        var entity = pointEntity;
        if (entity == null) {
            entity = SetPoint(newValue);
        } else {
            entity.ReplacePoint(newValue);
        }
    }

    public void RemovePoint() {
        pointEntity.Destroy();
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentEntityApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public partial class GameEntity {

    public PointComponent point { get { return (PointComponent)GetComponent(GameComponentsLookup.Point); } }
    public bool hasPoint { get { return HasComponent(GameComponentsLookup.Point); } }

    public void AddPoint(int newValue) {
        var index = GameComponentsLookup.Point;
        var component = (PointComponent)CreateComponent(index, typeof(PointComponent));
        component.value = newValue;
        AddComponent(index, component);
    }

    public void ReplacePoint(int newValue) {
        var index = GameComponentsLookup.Point;
        var component = (PointComponent)CreateComponent(index, typeof(PointComponent));
        component.value = newValue;
        ReplaceComponent(index, component);
    }

    public void RemovePoint() {
        RemoveComponent(GameComponentsLookup.Point);
    }
}

//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by Entitas.CodeGeneration.Plugins.ComponentMatcherApiGenerator.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
public sealed partial class GameMatcher {

    static Entitas.IMatcher<GameEntity> _matcherPoint;

    public static Entitas.IMatcher<GameEntity> Point {
        get {
            if (_matcherPoint == null) {
                var matcher = (Entitas.Matcher<GameEntity>)Entitas.Matcher<GameEntity>.AllOf(GameComponentsLookup.Point);
                matcher.componentNames = GameComponentsLookup.componentNames;
                _matcherPoint = matcher;
            }

            return _matcherPoint;
        }
    }
}
