using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour, IGameSessionListener, IPointListener
{
    [SerializeField] private TextMeshProUGUI _txtCountDown;
    
    [Space(5)]
    [SerializeField] private TextMeshProUGUI _txtPoint;
    [SerializeField] private TextMeshProUGUI _txtHighestPoint;

    [Space(5)]
    [SerializeField] private GameObject _playFieldUI;
    [SerializeField] private GameObject _countDownFieldUI;
    [SerializeField] private GameObject _endFieldUI;


    private int _highestPoint;

    private void Start()
    {
        _highestPoint = GameSaver.GetPoint();
        _txtHighestPoint.SetText($"H: {_highestPoint}");
    }

    private void OnDestroy()
    {
        GameSaver.SavePoint(_highestPoint);
    }

    // Function to call after adding this View to a GameEntity
    public void RegisterListeners(GameEntity entity)
    {   
        entity.AddPointListener(this);
        entity.AddGameSessionListener(this);
    }

    public void SetCountDown(int value)
    {
        _txtCountDown.SetText(value.ToString());
    }
    
    public void OnPoint(GameEntity entity, int value)
    {
        _txtPoint.SetText(value.ToString());
        HandleHighestPoint(value);
    }   

    private void HandleHighestPoint(int currentPoint)
    {
        if (currentPoint > _highestPoint)
        {
            _highestPoint = currentPoint;
            _txtHighestPoint.SetText($"H: {_highestPoint}");
        }
    }

    public void OnGameSession(GameEntity entity, GameStatus status)
    {
        _playFieldUI.SetActive(status == GameStatus.Start);
        _countDownFieldUI.SetActive(status == GameStatus.Prepare);
        _endFieldUI.SetActive(status == GameStatus.End);
    }
}