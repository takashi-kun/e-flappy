using UnityEngine;

public class GameSaver
{
    private const string POINT_KEY = "GameSaver.Point";
    
    public static int GetPoint()
    {
        return PlayerPrefs.GetInt(POINT_KEY, 0);
    }
    
    public static void SavePoint(int value)
    {
        if (value > 0)
        {
            PlayerPrefs.SetInt(POINT_KEY, value);
        }
    }
}