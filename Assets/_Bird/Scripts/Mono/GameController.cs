using System.Collections;
using Entitas;
using Entitas.Unity;
using UnityEngine;

public class GameController : MonoBehaviour, IGameSessionListener
{
    public GameObject goBird;

    [Header("UI")] 
    [SerializeField] private UIManager _uiManager;
    
    // Private fields
    private Systems _systems;   
    private Contexts _contexts;
    private GameContext _game;

    private Vector3 _birdStartPosition;

    void Start()
    {
        _contexts = Contexts.sharedInstance;
        _game = _contexts.game;
        
        _systems = CreateSystems(_contexts);
        _systems.Initialize();
        
        // Create entities
        CreateGameSession();
        AddListeners();
        
        //
        Caching();
    }

    void Update()
    {
        _systems.Execute();
        _systems.Cleanup();
    }

    private Systems CreateSystems(Contexts contexts)
    {
        return new Feature("Systems")    
            .Add(new InputSystems(contexts))
            .Add(new AudioSystems(contexts))
            .Add(new GameSystems(contexts));
    }

    private void CreateGameSession()
    {
        var session = _game.CreateEntity();
        session.AddGameSession(GameStatus.Start);
        session.AddPoint(0);
        
        _game.isControllable = false;
    }

    private void AddListeners()
    {
        _uiManager.RegisterListeners(_game.gameSessionEntity);
        _game.gameSessionEntity.AddGameSessionListener(this);
    }

    private void Caching()
    {
        _birdStartPosition = goBird.transform.position;
    }

    public void StartGame()
    {
        _game.ReplaceGameSession(GameStatus.Prepare);
        StartCoroutine(CoPreparing());
    }

    private IEnumerator CoPreparing()
    {
        int countDown = 3;      
        _uiManager.SetCountDown(countDown);
        
        while (countDown > 0)
        {
            yield return new WaitForSeconds(1f);
            countDown--;
            _uiManager.SetCountDown(countDown);

            if (countDown == 0)
            {
                _game.isControllable = true;
                _game.ReplaceGameSession(GameStatus.Playing);
                yield break;
            }
        }
    }

    public void ReplayGame()
    {
        _game.ReplacePoint(0);
        goBird.transform.position = _birdStartPosition;
        
        StartGame();
    }

    public void OnGameSession(GameEntity entity, GameStatus status)
    {
        if (status == GameStatus.Playing)
        {
            CreateBirdEntity();
        }
    }
    
    private void CreateBirdEntity()
    {
        var entityBird = _contexts.game.CreateEntity();
        entityBird.isBird = true;
        entityBird.AddView(goBird);
        entityBird.AddVelocity(Vector3.zero);

        var birdCollider = goBird.GetComponent<Collider2D>();
        var birdRay = new RayDirection[] {RayDirection.Up, RayDirection.Down, RayDirection.Right};
        entityBird.AddRay(birdCollider, birdRay, 0.35f);  
        
        goBird.Link(entityBird);
    }
}