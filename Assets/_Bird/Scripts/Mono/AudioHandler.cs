using UnityEngine;

public class AudioHandler : MonoBehaviour
{
    public static AudioHandler Instance;

    [SerializeField] private AudioSource _audioSource;

    [SerializeField] private AudioClip _hit;
    [SerializeField] private AudioClip _point;
    [SerializeField] private AudioClip _flyTap;
    
    private void Awake()
    {
        Instance = this;
    }

    public void PlayHit()
    {
        _audioSource.PlayOneShot(_hit);
    }
    
    public void PlayPoint()
    {
        _audioSource.PlayOneShot(_point);
    }
    
    public void PlayFlyTap()
    {
        _audioSource.PlayOneShot(_flyTap);
    }
}