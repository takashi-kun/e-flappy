using Entitas;

public class InputSystems : Feature
{
    public InputSystems(Contexts contexts) : base("Input Systems")
    {
        Add(new EmitInputSystem(contexts));
        Add(new CommandJumpSystem(contexts));
        
        // clean up
        Add(new InputCleanupSystems(contexts));
    }         
}