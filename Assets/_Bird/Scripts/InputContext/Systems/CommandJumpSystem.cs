using System.Collections.Generic;
using Entitas;

public class CommandJumpSystem : ReactiveSystem<InputEntity>
{
    private IGroup<GameEntity> _group;
    public CommandJumpSystem(Contexts contexts) : base(contexts.input)
    {
        _group = contexts.game.GetGroup(GameMatcher.Bird);
    }

    protected override ICollector<InputEntity> GetTrigger(IContext<InputEntity> context)
    {
        return context.CreateCollector(InputMatcher.Touched);
    }

    protected override bool Filter(InputEntity entity)
    {
        return entity.isTouched;
    }

    protected override void Execute(List<InputEntity> entities)
    {
        foreach (var e in _group)
        {
            e.isJump = true;
        }
    }
}