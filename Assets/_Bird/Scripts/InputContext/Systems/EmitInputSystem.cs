using Entitas;
using UnityEngine;

public class EmitInputSystem : IInitializeSystem, IExecuteSystem
{
    private Camera _camera;
    private readonly InputContext _context;
    private InputEntity _inputEntity;

    public EmitInputSystem(Contexts contexts)
    {
        _context = contexts.input;
    }

    public void Initialize()
    {
        _camera = Camera.main;
        _context.isInput = true;

        _inputEntity = _context.inputEntity;
    }

    public void Execute()
    {
        if (UnityEngine.Input.GetMouseButtonDown(0))
        {
            _inputEntity.isTouched = true;
        }
    }
}